#!/bin/bash
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install mysql-server
sudo apt-get install mysql-server php5-mysql
sudo mysql_install_db
sudo mysql_secure_installation
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt php5-gd php5-xdebug php5-curl
sudo apt-get install phpmyadmin
sudo ln -s /usr/share/phpmyadmin /var/www/html/
sudo php5enmod mcrypt
sudo service apache2 restart

#install xdebug
sudo apt-get install php5-xdebug
#sudo apt-get install php5-pecl, php5-dev
#sudo apt-get install php-pear
#sudo pecl install xdebug

#sudo apt-get update; sudo apt-get install gnome-session-fallback
#sudo apt-add-repository ppa:webupd8team/java
#sudo apt-get update
#sudo apt-get install oracle-java8-installer